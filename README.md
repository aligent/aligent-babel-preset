# Aligent Babel Config Present

Simply install via `npm i -D @aligent/babel-preset`

Then add the following to you `babel.config.js` or `.babelrc.json`

```json
{
  "presets": ["@aligent/babel-preset"]
}
```



### You can also configure the preset to pass options through to @babel/preset-env

```js
module.exports = {
    presets: [
        [
            '@aligent/babel-preset',
            // same config here will be merge with default and provided to @babel/preset-env
            {
                shippedProposals: true,
                useBuiltIns: 'entry',
                modules: false,
                corejs: '3'
            }
        ]
    ]
};
```


## Update

Once updated the config, run `npm test` and check inside project too.

- Update changelog and commit
- Run `npm version patch|minor|major`
- Git push (--tags)
- npm publish
