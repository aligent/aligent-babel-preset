# Aligent Babel Preset

# 1.3.0

- Remove babel plugins that are automatically included through `@babel/preset-env`.

- Update remaining packages to latest minor versions

# 1.2.0

- Update to core-js 3.6.0

# 1.1.1

- Remove console.log's from output

# 1.1.0

- Allow customising of @babel/present-env by passing config object through @aligent/babel-preset
- Better support for jest test transforms

# 1.0.2

- Fix config to support caching and exporting in correct preset format.

# 1.0.1

- Add tests for react jsx
- Add core js version

# 1.0.0

- Initial release.
