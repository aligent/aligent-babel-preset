const pluginSyntaxDynamicImport = require('@babel/plugin-syntax-dynamic-import');
const presetEnv = require('@babel/preset-env');
const presetReact = require('@babel/preset-react');

const defaultConfig = {
    useBuiltIns: 'entry',
    shippedProposals: true,
    corejs: '3',
    modules: false
};

// Test mode requires modules on and targeting node
const testConfig = {
    modules: 'auto',
    targets: {
        node: 'current'
    }
};

module.exports = (api, config) => {
    api.cache.using(() => process.env.NODE_ENV);
    const testMode = process.env.NODE_ENV === 'test';

    const envConfig = {
        ...defaultConfig,
        ...(testMode && testConfig),
        ...config
    };

    // Uncomment the following 2 lines if environment feedback is required - can be done while inside of node_modules
    // console.log('<> ALIGENT BABEL MODE <>', process.env.NODE_ENV);
    // console.log('<> ALIGENT BABEL CONFIG <>', envConfig);

    const presets = [[presetEnv, envConfig], presetReact];
    const plugins = [
        pluginSyntaxDynamicImport
    ];
    return { plugins, presets };
};
